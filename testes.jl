using Test
include("miniep11.jl")

function test()
	println("Testing")
	@test palindromo("oio")
	@test palindromo("")
	@test palindromo("Socorram-me, subi no ônibus em Marrocos!")
	@test !palindromo("oi")
	println("Ok")
end

test()

