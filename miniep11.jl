function rmSpecialChars(str)
	str = lowercase(str)
	i = firstindex(str)
	while i <= lastindex(str)
		if 'a' <= str[i] <= 'z'
			i = nextind(str, i)
			continue
		end

		subs = Dict(
			'ç' => "c",
			'ü' => "u",
			'ö' => "o",
			'ï' => "i",
			'ë' => "e",
			'ä' => "a",
			'õ' => "o",
			'ã' => "a",
			'û' => "u",
			'ô' => "o",
			'î' => "i",
			'ê' => "e",
			'â' => "a",
			'ò' => "o",
			'ò' => "o",
			'ì' => "i",
			'è' => "e",
			'à' => "a",
			'ú' => "u",
			'ó' => "o",
			'í' => "i",
			'é' => "e",
			'á' => "a")

		nStr = str[1 : prevind(str, i)]
		nStr *= get(subs, str[i], "")
		nStr *= str[nextind(str, i) : lastindex(str)]
		str = nStr
	end
	return str
end

function palindromo(str)
	str = rmSpecialChars(str)
	i = firstindex(str)
	j = lastindex(str)
	while i < j
		if str[i] != str[j]
			return false
		end
		i = nextind(str, i)
		j = prevind(str, j)
	end
	return true
end
